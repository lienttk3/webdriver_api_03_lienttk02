package webdriver_api_03_package01;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class TC09_Enable {
	WebDriver driver;
	  
	  @BeforeClass
	  public void beforeClass() {
		// Open browser//,
					driver = new FirefoxDriver();
	  }
	  
	  @Test
	  public void Test01_TextboxIsEnable() throws Exception {
			// Open app
			driver.get("http://daominhdam.890m.com/");
			
			// Wait page load successfully
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			// Maximize browser fullscreen//
			driver.manage().window().maximize();
			
			String element = "//*[@id='edu']";
			 if(isElementEnable(driver, element)){
			 System.out.println("Textbox is enable");
			 } else{
			 System.out.println("Textbox isn't enable");
			 }
	  }
	  private boolean isElementEnable(WebDriver driver2, String element) {
		// TODO Auto-generated method stub
		  try {
			  WebElement locator;
			  locator=driver2.findElement(By.xpath(element));
			  return locator.isEnabled();
		  }catch(NoSuchElementException e) {
			  return false;
		  }
	}

	@AfterClass
	  public void afterClass() {
		driver.quit();
	  }


}
