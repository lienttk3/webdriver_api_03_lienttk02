package webdriver_api_03_package01;

import org.testng.annotations.Test;

import com.thoughtworks.selenium.condition.Text;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;

import static org.junit.Assert.assertArrayEquals;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic02_webdriver {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		// Open browser
		driver = new FirefoxDriver();
		
		//System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		//driver= new ChromeDriver();
		
		//System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		//driver= new InternetExplorerDriver();
	}
	@BeforeMethod
	public void dataForEachTestCase() {
		
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		driver.manage().window().maximize();
    	String homePagetitle=driver.getTitle();
		Assert.assertEquals("Home page", homePagetitle);
		
		WebElement myAccountLink=
				driver.findElement(By.xpath("//div[@class='footer-container']//a[@title='My Account']"));
		myAccountLink.click();	
		
	}
    @Test
	public void TC_01_VerifyUrlAndTitle() {
    	
		// Open app
    	WebElement createAccount=driver.findElement(By.xpath("//a[@title=\"Create an Account\"]"));
    	createAccount.click();
    	driver.navigate().back();
    	
    	String loginUrl=driver.getCurrentUrl();
    	Assert.assertEquals("http://live.guru99.com/index.php/customer/account/login/", loginUrl);
    	
    	driver.navigate().forward();
    	
    	String CheckUrlCreateAcc=driver.getCurrentUrl();
    	Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", CheckUrlCreateAcc);
    	
	}
    @Test
   	public void TC_02_LoginWithEmptyField() {
   
    	WebElement loginButton = driver.findElement(By.xpath("//*[@id='send2']"));
    	loginButton.click();
    	
    	WebElement ErrorMessageUser=driver.findElement(By.xpath("//*[@id='advice-required-entry-email']"));
    	Assert.assertEquals("This is a required field.", ErrorMessageUser.getText());
    	
    	WebElement ErrorMessagePass=driver.findElement(By.xpath("//*[@id='advice-required-entry-pass']"));
    	Assert.assertEquals("This is a required field.", ErrorMessagePass.getText());
    	
    }
    
    @Test
   	public void TC_03_LoginWithInvalidEmail() {
    	WebElement invalidEmail = driver.findElement(By.xpath("//input[@id='email']"));
    	invalidEmail.sendKeys("123434234@12312.123123");
    	
    	WebElement loginButton = driver.findElement(By.xpath("//*[@id='send2']"));
    	loginButton.click();
    	

    	WebElement ErrorMessageEmail=driver.findElement(By.xpath("//div[@id='advice-validate-email-email']"));
    	Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.", ErrorMessageEmail.getText());
    	
    }
    @Test
   	public void TC_04_LoginWithInvalidPass() {
    	WebElement correctEmail = driver.findElement(By.xpath("//input[@id='email']"));
    	correctEmail.sendKeys("automation@gmail.com");
    	
    	WebElement incorrectPass = driver.findElement(By.xpath("//input[@id='pass']"));
    	incorrectPass.sendKeys("123");
    	
    	WebElement loginButton = driver.findElement(By.xpath("//*[@id='send2']"));
    	loginButton.click();
    	
    	WebElement ErrorMessagePass=driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']"));
    	Assert.assertEquals("Please enter 6 or more characters without leading or trailing spaces.", ErrorMessagePass.getText());
    }
    
    public int randomData() {
        Random rand = new Random();
        int number = rand.nextInt(999999) + 1;
        return number;
        
       
   }
    
    //String Email= "automation" + randomData() + "@gmail.com";
    
    @Test
   	public void TC_05_CreateAnAccount() {
    	WebElement createAccount=driver.findElement(By.xpath("//a[@title=\"Create an Account\"]"));
    	createAccount.click();
    	
    	WebElement FirstName = driver.findElement(By.xpath("//input[@id='firstname']"));
    	FirstName.sendKeys("kim");
    	
    	WebElement LastName = driver.findElement(By.xpath("//input[@id='lastname']"));
    	LastName.sendKeys("lien");
    	
    	WebElement Email= driver.findElement(By.xpath("//input[@id='email_address']"));
    	Email.sendKeys("kimlien30100@gmail.com");
    	
    	WebElement Password = driver.findElement(By.xpath("//input[@id='password']"));
    	Password.sendKeys("abcde12345");
    	
    	WebElement ConfirmPassword = driver.findElement(By.xpath("//input[@id='confirmation']"));
    	ConfirmPassword.sendKeys("abcde12345");
    	
    	WebElement registerButton =driver.findElement(By.xpath("//button[@title='Register']"));
    	registerButton.click();
    	
    	WebElement registeringMessage=driver.findElement(By.xpath("//span[contains(text(),'Thank you')]"));
    	Assert.assertEquals("Thank you for registering with Main Website Store.", registeringMessage.getText());
    	
    	WebElement Account=driver.findElement(By.xpath("//*[@id='header']//span[contains(text(),'Account')]"));
    	Account.click();
    	
    	WebElement logout=driver.findElement(By.xpath("//*[@title='Log Out']"));
    	logout.click();
        
        // Changed by Dam Dao
    	
    }
    
	@AfterClass             
	public void afterClass() {
		// Close browser
		driver.quit();
	}

}
	