package webdriver_api_03_package01;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;


public class templateTestcase {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		// Open browser
		driver = new FirefoxDriver();		
	}
	@Test
	public void TC_01() {
		// Open app
				driver.get("http://live.guru99.com/");

				// Wait page load successfully
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				// Maximize browser fullscreen
				driver.manage().window().maximize();
		// Get current title
		String homePageTitle = driver.getTitle();

		// Verify actual title with expected title
		Assert.assertEquals("Home page", homePageTitle);
	}

	@AfterClass
	public void afterClass() {
		// Close browser
		driver.quit();
	}

}

